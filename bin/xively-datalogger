#!/usr/bin/env bash

# Xively Feed Updater
# https://raw.github.com/martinclaro/raspberrypi-tools/master/bin/xively-datalogger

# Default values
CONFIG=~/.xivelyrc
NET_ST_FILE=/tmp/xively.net.stats
APIKEY=
FEED=
SIMULATION=0
DEBUG=0
CURL_OPTS=
PROTO=http

usage()
{
cat << EOF
usage: $0 options

Xively Feed Updater (https://xively.com/).

OPTIONS:
    -h      Show this message
    -c file Config file (default: ${CONFIG})
    -f id   Feed Id (optional)
    -k key  API Key (optional)
    -s      Simulation
    -d      Debug mode
EOF
}

while getopts "hc:f:k:sd" OPTION
do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        c)
            CONFIG=${OPTARG}
            ;;
        f)
            FEED=${OPTARG}
            ;;
        k)
            APIKEY=${OPTARG}
            ;;
        s)
            SIMULATION=1
            ;;
        d)
            DEBUG=1
            ;;
        ?)
            usage
            exit 1
            ;;
    esac
done


# Configuration Profile
if [ ! -f "${CONFIG}" ]; then
    echo "XIVELY_API_KEY=${APIKEY}" > ${CONFIG}
    echo "XIVELY_API_FEED=${FEED}" >> ${CONFIG}
    echo "XIVELY_API_LOCATION_LAT=\"0\"" >> ${CONFIG}
    echo "XIVELY_API_LOCATION_LONG=\"0\"" >> ${CONFIG}
    echo "XIVELY_API_LOCATION_ELEVATION=0" >> ${CONFIG}
    echo "XIVELY_API_HTTPS=1" >> ${CONFIG}
    echo "XIVELY_API_IPV4_ONLY=1" >> ${CONFIG}
    echo "INFO: Configuration file created at: ${CONFIG}"
fi
. ${CONFIG}

# Configuration Overrides
if [ "x${APIKEY}" != "x" ]; then
    XIVELY_API_KEY=${APIKEY}
fi
if [ "x${FEED}" != "x" ]; then
    XIVELY_API_FEED=${FEED}
fi
if [ "x${XIVELY_API_HTTPS}" == "x1" ]; then
    PROTO=https
fi

# Defaults
if [ "x${XIVELY_API_LOCATION_DISPOSITION}" == "x" ]; then
    XIVELY_API_LOCATION_DISPOSITION=fixed
fi
if [ "x${XIVELY_API_LOCATION_EXPOSURE}" == "x" ]; then
    XIVELY_API_LOCATION_EXPOSURE=indoor
fi
if [ "x${XIVELY_API_LOCATION_DOMAIN}" == "x" ]; then
    XIVELY_API_LOCATION_DOMAIN=physical
fi

DATAFILE=`mktemp /tmp/xively_request.XXXXXX`
RESPFILE=`mktemp /tmp/xively_response.XXXXXX`
CURLFILE=`mktemp /tmp/xively_curllog.XXXXXX`

RPI_HOSTNAME=`hostname`

# CPU Temp
CPU_TEMP=`cat /sys/class/thermal/thermal_zone0/temp | awk '{printf "%0.2f", $1/1000}'`

# CPU Temp
GPU_TEMP=`/opt/vc/bin/vcgencmd measure_temp | cut -d= -f2 | cut -d\' -f1 | awk '{printf "%0.2f", $1}'`

# CPU Load
CPU_LOAD_ST=`cat /proc/loadavg`
CPU_LOAD_01=`echo "${CPU_LOAD_ST}" | awk '{print $1}'`
CPU_LOAD_05=`echo "${CPU_LOAD_ST}" | awk '{print $2}'`
CPU_LOAD_15=`echo "${CPU_LOAD_ST}" | awk '{print $3}'`

# CPU Usage
CPU_STAT=`vmstat 2 3 | tail -n1 | awk '{print $13" "$14" "$15" "$16}'`
if [ "X${CPU_STAT}" != "X" ]; then
    CPU_IDLE=`echo ${CPU_STAT} | awk '{print $3}'`
    CPU_USAGE=`echo "100 ${CPU_IDLE}" | awk '{printf "%0.2f", ($1 - $2)}'`
fi

# Memory
MEM_STATUS=`free -b | grep "^Mem"`
MEM_TOTAL=`echo "${MEM_STATUS}" | awk '{printf "%0.2f", $2/1048576}'`
MEM_USED=`echo "${MEM_STATUS}" | awk '{printf "%0.2f", ($3 - ($6 + $7))/1048576}'`
MEM_FREE=`echo "${MEM_STATUS}" | awk '{printf "%0.2f", ($4 + ($6 + $7))/1048576}'`
MEM_CACH=`echo "${MEM_STATUS}" | awk '{printf "%0.2f", ($6 + $7)/1048576}'`

# Network
# InOctets OutOctets
# 829107479 157935551
NET_STATS=`cat /proc/net/netstat | grep 'IpExt: ' | awk '{print $8" "$9}' | tail -n 1`
NET_TS=`date "+%s"`
NET_DS=`echo "${NET_STATS}" | awk '{print $1}'`
NET_US=`echo "${NET_STATS}" | awk '{print $2}'`

if [ -f "${NET_ST_FILE}" ]; then
    . ${NET_ST_FILE}
    NET_TS_DELTA=$[ ${NET_TS} - ${NET_TS_OLD} ]
    NET_DS_DELTA=$[ ${NET_DS} - ${NET_DS_OLD} ]
    NET_US_DELTA=$[ ${NET_US} - ${NET_US_OLD} ]
    NET_DS_CURR=`awk 'BEGIN {printf("%0.2f", (('$NET_DS_DELTA' / '$NET_TS_DELTA')/1024)*8)}'`
    NET_US_CURR=`awk 'BEGIN {printf("%0.2f", (('$NET_US_DELTA' / '$NET_TS_DELTA')/1024)*8)}'`
else
    NET_DS_CURR=0
    NET_US_CURR=0
fi


cat /dev/null > ${NET_ST_FILE}
echo "NET_TS_OLD=${NET_TS}" >> ${NET_ST_FILE}
echo "NET_DS_OLD=${NET_DS}" >> ${NET_ST_FILE}
echo "NET_US_OLD=${NET_US}" >> ${NET_ST_FILE}

cat <<EOF>> ${DATAFILE}
{
    "title": "Raspberry Pi: ${RPI_HOSTNAME}",
    "tags": [ "monitoring", "raspberrypi" ],
    "location": {
        "name": "${RPI_HOSTNAME}",
        "disposition": "${XIVELY_API_LOCATION_DISPOSITION}",
        "lat": "${XIVELY_API_LOCATION_LAT}",
        "lon": "${XIVELY_API_LOCATION_LONG}",
        "ele": "${XIVELY_API_LOCATION_ELEVATION}",
        "exposure": "${XIVELY_API_LOCATION_EXPOSURE}",
        "domain": "${XIVELY_API_LOCATION_DOMAIN}"
    },
    "datastreams": [
        { "current_value": "${CPU_TEMP}",
            "id": "cpu_temp",
            "tags": [ "cpu", "temperature" ],
            "unit": { "symbol": "\u00baC", "label": "CPU Temp" }
        },
        { "current_value": "${GPU_TEMP}",
            "id": "gpu_temp",
            "tags": [ "gpu", "temperature" ],
            "unit": { "symbol": "\u00baC", "label": "GPU Temp" }
        },
        { "current_value": "${CPU_USAGE}",
            "id": "cpu_usage",
            "tags": [ "cpu", "load" ],
            "unit": { "symbol": "\u0025", "label": "CPU Usage" }
        },
        { "current_value": "${CPU_LOAD_01}",
            "id": "cpu_load_01",
            "tags": [ "cpu", "load" ],
            "unit": { "symbol": "", "label": "CPU Load 1m" }
        },
        { "current_value": "${CPU_LOAD_05}",
            "id": "cpu_load_05",
            "tags": [ "cpu", "load" ],
            "unit": { "symbol": "", "label": "CPU Load 5m" }
        },
        { "current_value": "${CPU_LOAD_15}",
            "id": "cpu_load_15",
            "tags": [ "cpu", "load" ],
            "unit": { "symbol": "", "label": "CPU Load 15m" }
        },
        { "current_value": "${MEM_FREE}",
            "id": "mem_free",
            "tags": [ "memory" ],
            "unit": { "symbol": "MB", "label": "Memory Free" }
        },
        { "current_value": "${MEM_USED}",
            "id": "mem_used",
            "tags": [ "memory" ],
            "unit": { "symbol": "MB", "label": "Memory Used" }
        },
        { "current_value": "${MEM_CACH}",
            "id": "mem_cached",
            "tags": [ "memory" ],
            "unit": { "symbol": "MB", "label": "Memory Cached" }
        },
        { "current_value": "${NET_DS_CURR}",
            "id": "net_downstream",
            "tags": [ "network" ],
            "unit": { "symbol": "Kbps", "label": "Dowstream Speed" }
        },
        { "current_value": "${NET_US_CURR}",
            "id": "net_upstream",
            "tags": [ "network" ],
            "unit": { "symbol": "Kbps", "label": "Upstream Speed" }
        }
    ]
}
EOF

if [ ${DEBUG} -gt 0 ]; then
    cat ${DATAFILE}
fi

if [ ${SIMULATION} -eq 0 ]; then
    if [ "X${XIVELY_API_IPV4_ONLY}" == "X1" ]; then
        CURL_OPTS="-4"
    fi
    curl ${CURL_OPTS} --request PUT --data-binary @${DATAFILE} --header "X-ApiKey: ${XIVELY_API_KEY}" -o "${RESPFILE}" --verbose "${PROTO}://api.xively.com/v2/feeds/${XIVELY_API_FEED}" 1>${CURLFILE} 2>&1
    XIVELY_RESULT=$?

    if [ ${DEBUG} -gt 0 ]; then
        cat "${CURLFILE}"
        cat "${RESPFILE}"
        echo ""
    fi
else
    XIVELY_RESULT=0
fi

rm -f "${DATAFILE}" "${RESPFILE}" "${CURLFILE}"

exit ${XIVELY_RESULT}
